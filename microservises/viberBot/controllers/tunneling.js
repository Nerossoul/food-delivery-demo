const axios = require("axios");

let serverUrl = "http://localhost:5000/api/viber";

async function getMessageResponce(message, userProfile) {
  return new Promise((resolve, reject) => {
    const messageData = {
      message,
      userProfile
    };
    axios
      .post(serverUrl + "/message", messageData)
      .then(
        resp => {
          if (resp.data) {
            // eslint-disable-next-line no-console
            console.log("GOOD getMessageResponce", resp.data);
            resolve(resp.data);
          } else {
            // eslint-disable-next-line no-console
            console.log("ERROR 1 getMessageResponce");
            reject(false);
          }
        },
        err => {
          console.log("ERROR 2 getMessageResponce", err);
          reject(false);
        }
      )
      .catch(err => {
        // eslint-disable-next-line no-console
        console.log("unhandled error!", err);
        reject(false);
      });
  });
}

module.exports = { getMessageResponce };
