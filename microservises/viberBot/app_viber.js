const ViberBot = require("viber-bot").Bot,
  BotEvents = require("viber-bot").Events,
  getMessageResponce = require("./controllers/tunneling").getMessageResponce;
const TextMessage = require("viber-bot").Message.Text;
const UrlMessage = require("viber-bot").Message.Url;
const ContactMessage = require("viber-bot").Message.Contact;
const PictureMessage = require("viber-bot").Message.Picture;
const VideoMessage = require("viber-bot").Message.Video;
const LocationMessage = require("viber-bot").Message.Location;
const StickerMessage = require("viber-bot").Message.Sticker;
const RichMediaMessage = require("viber-bot").Message.RichMedia;
const KeyboardMessage = require("viber-bot").Message.Keyboard;
const ngrok = require("./controllers/get_public_url");
const express = require("express");
const app = express();

const lunchPreloader = async response => {
  const KILL_KEYBOARD = {
    Type: "keyboard",
    Buttons: [
      {
        Columns: 6,
        Rows: 2,
        ActionType: "reply",
        ActionBody: "loading",
        BgColor: "#f7bb3f",
        BgMediaType: "gif",
        BgMediaScaleType: "fill",
        BgMedia:
          "https://cdn.dribbble.com/users/645440/screenshots/3266490/loader-2_food.gif"
      }
    ]
  };
  //
  const keyboardMessage = new KeyboardMessage(KILL_KEYBOARD);
  // console.log(message);
  await response.send(keyboardMessage).then(
    result => console.log("KILL KEYBOARD INFO", result),
    error => console.log("ERROR KILLING KEYBOARD", error)
  );
};

const bot = new ViberBot({
  authToken: "4a800335c5a7d641-125a1b81a9e0a5b-ddd47ae35ca935c5", //developer local server bot token
  name: "nerossoultest2",
  avatar: "https://upload.wikimedia.org/wikipedia/commons/3/3d/Katze_weiss.png"
});
let profile = response =>
  bot.getUserDetails(response.userProfile).then(
    userDetails => console.log(userDetails),
    error => console.log("ERROR getUserDetails", error)
  );
bot.on(BotEvents.SUBSCRIBED, async response => {
  console.log("--->SUBSCRIBED");
  const SUBSCRIBE_KEYBOARD = {
    Type: "keyboard",
    Buttons: [
      {
        Columns: 6,
        Rows: 2,
        Text: '<br><font color="#0000FF"><b>Начать пользоваться</b></font>',
        TextSize: "large",
        TextHAlign: "center",
        TextVAlign: "middle",
        ActionType: "reply",
        ActionBody: '{"type": "SUBSCRIBE"}',
        BgColor: "#f7bb3f"
      }
    ]
  };
  await lunchPreloader(response);
  response.send(
    new TextMessage(
      `Добрый день ${response.userProfile.name}. Это приложение доставки обедов на дом`,
      SUBSCRIBE_KEYBOARD
    )
  );
});

bot.on(BotEvents.MESSAGE_RECEIVED, async (message, response) => {
  console.log("--->MESSAGE_RECEIVED");
  await lunchPreloader(response);
  let serviceResponce = await getMessageResponce(message, response.userProfile);
  let SAMPLE_KEYBOARD = {
    Type: "keyboard",
    Buttons: [
      {
        Columns: 6,
        Rows: 1,
        Text: '<br><font color="#494E67"><b>Посмотреть меню</b></font>',
        TextSize: "large",
        TextHAlign: "center",
        TextVAlign: "middle",
        ActionType: "reply",
        ActionBody: '{"type": "SUBSCRIBE"}',
        BgColor: "#f7bb3f"
      },
      {
        Columns: 6,
        Rows: 1,
        Text: '<br><font color="#494E67"><b>Заказать доставку</b></font>',
        TextSize: "large",
        TextHAlign: "center",
        TextVAlign: "middle",
        ActionType: "reply",
        ActionBody: "Order",
        BgColor: "#7eceea"
      }
    ]
  };

  responseMessage = "";
  if (serviceResponce.message) {
    responseMessage = serviceResponce.message;
  } else {
    responseMessage = "Ошибка попробуйте ещё раз";
  }
  if (serviceResponce.keyboard) {
    SAMPLE_KEYBOARD = serviceResponce.keyboard;
  }
  response.send(new TextMessage(responseMessage, SAMPLE_KEYBOARD)).then(
    result => console.log("MESSAGE INFO", result),
    error => console.log("ERROR SENDING MESSAGE", error)
  );
});

// UNSUBSCRIBED

bot.on(BotEvents.UNSUBSCRIBED, response => {
  console.log("--> UNSUBSCRIBED");
});

// run viber bot server

const port = process.env.PORT || 5001;
console.log("port: ", port);
app.use("/viberbot/", bot.middleware());
app.listen(port, () => {
  if (process.env.NODE_ENV === "production") {
    console.log("production");
    function setItBlya() {
      console.log("Setting web hook now");
      let viberWebHook = "https://lunchgood.ru/viberbot/";
      bot
        .setWebhook(viberWebHook)
        .then(result => {
          console.log(result);
          console.log(`VIBER BOT SERVICE STARTED ON ${port}...`);
        })
        .catch(error => {
          console.log(
            "Can not set webhook on following server" +
              viberWebHook +
              ". Is it running?"
          );
          console.error(error);
          process.exit(1);
        });
    }
    setTimeout(setItBlya, 5000);
  } else {
    ngrok
      .getPublicUrl()
      .then(publicUrl => {
        viberWebHookServerUrl = publicUrl + "/viberbot/";
        console.log('Set the new webhook to"', viberWebHookServerUrl);
        return viberWebHookServerUrl;
      })
      .then(
        viberWebHookServerUrl => {
          bot
            .setWebhook(viberWebHookServerUrl)
            .then(result => {
              console.log(result);
              console.log(`VIBER BOT SERVICE STARTED ON ${port}...`);
            })
            .catch(error => {
              console.log(
                "Can not set webhook on following server. Is it running?"
              );
              console.error(error);
              process.exit(1);
            });
        },
        error => {
          console.log("Can not connect to ngrok server. Is it running?");
          console.error(error);
        }
      )
      .catch(error => {
        console.log("Uncought error!!!");
        console.error(error);
      });
  }
});
