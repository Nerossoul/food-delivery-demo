## Heroku deploy instructions

### Before start or step zero
1. Install Heroku CLI
   https://devcenter.heroku.com/articles/heroku-cli#download-and-install

   or use next command

   ```
   curl https://cli-assets.heroku.com/install.sh | sh
   ```
2. Login to Heroku CLI
    ```
    heroku login
    ```


### 1. Clone the repository from heroku

```
heroku git:clone -a erts
```
this command creates folder **/erts** in current folder

### 2. Install all dependences

1. install node js dependences 
    go to **/erts** folder and type next command
    ```
    npm install
2. install vue js dependences
    go to **/erts/client** folder and type next command
    ```
    npm install
    ```

### 3. Run local servers

1. node js sever: go to **/erts** folder and type next command
    ```
    npm run serve
    ```
2. Vue js server: go to **/erts/client** folder and type next command
    ```
    npm run serve
    ```

### 4. DONE! You can work with code now.

### 5. Before deploy
go to **/erts/client** folder and type next command
```
npm run build
```
It creates files for deploy

### 6. Deploy

go to **/erts** folder and type next commands
```
git add .
```
```
git commit -m "your commit mark here"
```
```
git push heroku master
```

### 7. Pull

```
git pull heroku master
```
