// state machine functions
const Machine = require("xstate").Machine;
const State = require("xstate").State;
const actions = require("xstate").actions;

const send = require("xstate").send;
const sendParent = require("xstate").sendParent;
const interpret = require("xstate").interpret;
const spawn = require("xstate").spawn;

// state machine definition
const { stateMachineDefinition } = require("./stateMachineDefinition");
const { stateMachineOptions } = require("./stateMachineOptions");

// models functions
const { getIsMenuPassiveStatus } = require("../../db/models/config");
const { getActiveProducts } = require("../../db/models/products");
const { getAllCategories } = require("../../db/models/categories");
const {
  saveNewOrder,
  getActiveOrders,
  deleteOrder,
  updateOrder
} = require("../../db/models/orders");
const {
  getUserByViberId,
  createNewUser,
  updateUserByViberId
} = require("../../db/models/users");

// helpers
async function pushConfirmedOrderToDb(state) {
  let saveOrderResult = false;
  let editOrderResult = false;
  if (state.context.confirmedOrder.userId) {
    if (state.context.orderForEdit !== -1) {
      // update Order
      let orderId = state.context.orders[state.context.orderForEdit]._id;
      delete state.context.confirmedOrder._id;
      console.log(
        "======================================================================"
      );
      console.log(orderId);
      console.log(state.context.confirmedOrder);
      editOrderResult = await updateOrder(
        orderId,
        state.context.confirmedOrder
      );

      if (editOrderResult) {
        //clear state orders
        state.context.confirmedOrder = {};
        state.context.orderDraft.positions = [];
        state.context.orderForEdit = -1;
      }
    } else {
      // save new order
      saveOrderResult = await saveNewOrder(state.context.confirmedOrder);
      // updateOrder
      if (saveOrderResult) {
        //clear state orders
        state.context.confirmedOrder = {};
        state.context.orderDraft.positions = [];
        return state;
      } else {
        console.log("ERROR pushConfirmedOrderToDb", saveOrderResult);
      }
    }
  }
  return state;
}
async function deleteMarkedOrderFromDb(state) {
  if (state.context.orderForDelete !== -1) {
    let deleteOrderId = state.context.orders[state.context.orderForDelete]._id;
    let isOrderDeleted = deleteOrder(deleteOrderId);
    if (isOrderDeleted) {
      state.context.orderForDelete = -1;
      return state;
    }
  }
  return state;
}
async function getCurrentUserState(userProfile, viberSatateMachine) {
  let users = await getUserByViberId(userProfile.id);
  if (users.users.length === 0) {
    // create new user
    newUserObject = {
      userName: "",
      viberId: userProfile.id,
      viberUserName: userProfile.name,
      viberState: null,
      phones: [],
      addresses: []
    };
    await createNewUser(newUserObject);
    userData = newUserObject;
    return false;
  }

  if (users.users[0].viberState) {
    const stateDefinition = JSON.parse(users.users[0].viberState);
    const previousState = State.create(stateDefinition);
    const resolvedState = viberSatateMachine.resolveState(previousState);

    return resolvedState;
  }

  return false;
}

async function getCurrentUserData(userProfile) {
  let users = await getUserByViberId(userProfile.id);
  if (users.users.length === 0) {
    return {};
  } else {
    return users.users[0];
  }
}

function errorResponse(error, event, errorNumber) {
  console.log("====ERROR====");
  console.error(error);
  let message =
    "На сервере произошла ошибка #" +
    errorNumber +
    ". Попробуйте ещё раз. если ошибка будет повторяться сообщите нам пожалуйста об этом по номеру 8908214630";
  let keyboard = {
    Type: "keyboard",
    Buttons: [
      {
        Columns: 6,
        Rows: 2,
        Text: '<br><font color="#494E67"><b>Повторить команду</b></font>',
        TextSize: "large",
        TextHAlign: "center",
        TextVAlign: "middle",
        ActionType: "reply",
        ActionBody: JSON.stringify(event),
        BgColor: "#c75702"
      }
    ]
  };
  return {};
}
const getEventFromMessage = message => {
  let event = false;
  function IsJsonString(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }
  if (IsJsonString(message.text)) {
    event = JSON.parse(message.text);
  }
  if (event.type) {
    return event;
  }
  return false;
};
// state machine starting
async function getViberStateMachine() {
  console.log("START");
  const viberSatateMachine = Machine(
    stateMachineDefinition,
    stateMachineOptions
  );
  console.log("STARTED");

  return viberSatateMachine;
}
// ============= MAIN MAIN MAIN ==================================

async function applyStateMachineEvent(viberUserProfle, message) {
  const viberSatateMachine = await getViberStateMachine();
  let userData = await getCurrentUserData(viberUserProfle);
  console.log("IN CONTEXT");
  console.log(userData._id);
  let event = getEventFromMessage(message);

  let resolvedState = await getCurrentUserState(
    viberUserProfle,
    viberSatateMachine
  );
  if (!event) {
    // it just text let check may be it is registration
    if (resolvedState.value && resolvedState.value.registration) {
      if (resolvedState.value.registration === "waitingPhone") {
        let phone = event;
        event = { type: "SEND_PHONE", phone: message.text };
      }
      if (resolvedState.value.registration === "waitingAddress") {
        let phone = event;
        event = { type: "SEND_ADDRESS", address: message.text };
      }
    } else if (resolvedState.value && resolvedState.value.order) {
      if (resolvedState.value.order === "waitingAddressString") {
        let phone = event;
        event = { type: "SEND_ADDRESS_STRING", address: message.text };
      }
    }
  }

  if (event) {
    let isMenuPassive = await getIsMenuPassiveStatus();
    if (isMenuPassive.error) {
      return errorResponse(isMenuPassive.error, event, 501);
    }
    let activeProducts = await getActiveProducts();
    if (activeProducts.error) {
      return errorResponse(activeProducts.error, event, 502);
    }
    let categories = await getAllCategories();
    if (categories.error) {
      return errorResponse(categories.error, event, 503);
    }
    let avtiveOrders = [];
    if (userData._id) {
      avtiveOrders = await getActiveOrders(userData._id);
      if (avtiveOrders.error) {
        return errorResponse(avtiveOrders.error, event, 504);
      }
    }

    if (resolvedState) {
      resolvedState.context.isMenuPassive = isMenuPassive;
      resolvedState.context.activeProducts = activeProducts;
      resolvedState.context.categories = categories;
      resolvedState.context.orders = avtiveOrders;
      if (userData.phones) {
        resolvedState.context.phones = userData.phones;
      }
      if (userData.addresses) {
        resolvedState.context.addresses = userData.addresses;
      }
      if (userData._id) {
        resolvedState.context.orderDraft.userId = userData._id;
      }
      if (userData.viberUserName) {
        resolvedState.context.orderDraft.name = userData.viberUserName;
      }
    } else {
      viberSatateMachine.initialState.context.isMenuPassive = isMenuPassive;
      viberSatateMachine.initialState.context.activeProducts = activeProducts;
      viberSatateMachine.initialState.context.categories = categories;
      viberSatateMachine.initialState.context.orders = avtiveOrders;
      if (userData.phones) {
        viberSatateMachine.initialState.context.phones = userData.phones;
      }
      if (userData.addresses) {
        viberSatateMachine.initialState.context.addresses = userData.addresses;
      }
      if (userData._id) {
        viberSatateMachine.initialState.context.orderDraft.userId =
          userData._id;
      }
      if (userData.viberUserName) {
        viberSatateMachine.initialState.context.orderDraft.name =
          userData.viberUserName;
      }
    }

    let newState = {};
    let nextEvents = [];
    let currnetStateValue = "";
    let currentStateContext = {};
    const service = interpret(viberSatateMachine).onTransition(state => {
      newState = state;
      nextEvents = state.nextEvents;
      currnetStateValue = state.value;
      currentStateContext = state.context;
    });

    if (resolvedState) {
      service.start(resolvedState);
    } else {
      service.start();
    }

    service.send(event);

    service.stop();
    newState = await pushConfirmedOrderToDb(newState);
    newState = await deleteMarkedOrderFromDb(newState);
    let updateObject = {
      viberId: viberUserProfle.id,
      newUserData: {
        phones: newState.context.phones,
        addresses: newState.context.addresses,
        viberState: JSON.stringify(newState)
      }
    };
    updateUserByViberId(updateObject);
    console.log("=======RESULT=======");
    console.log("currnetStateValue", currnetStateValue);
    console.log("currentStateContext", currentStateContext);
    console.log("nextEvents", nextEvents);
    return {
      message: currentStateContext.message,
      keyboard: currentStateContext.keyboard
    };
  } else {
    console.log("It is just text MESSAGE use KEYBOARD PLEASE");
    let resolvedState = await getCurrentUserState(
      viberUserProfle,
      viberSatateMachine
    );
    if (resolvedState) {
      return {
        message:
          resolvedState.context.message +
          "_______________\nВы написали сообщение в текстовую строку. Пожалуйста используйте наше меню для взаимодействия с сервисом",
        keyboard: resolvedState.context.keyboard
      };
    }
    return {
      message:
        viberSatateMachine.initialState.context.message +
        "_______________\nВы написали сообщение в текстовую строку. Пожалуйста используйте наше меню для взаимодействия с сервисом",
      keyboard: viberSatateMachine.initialState.context.keyboard
    };
  }
}

module.exports = { applyStateMachineEvent };
