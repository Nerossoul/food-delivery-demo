const assign = require("xstate").assign;
function isConfirmedOrdersToThisAddressExists(context, address) {
  let goodOrders = context.orders.filter(order => {
    return order.address === address;
  });
  return goodOrders.length > 0;
}

function createComboCategoryRecomendation(context) {
  let necessaryComboCategories = {};
  let orderProductsFullInfo = context.orderDraft.positions.map(position => {
    let activeProduct = context.activeProducts.filter(activeProduct => {
      return activeProduct._id == position.productId;
    });
    necessaryComboCategories[activeProduct[0].comboCategoryId] = {
      left: 0,
      right: 0
    };
    return activeProduct[0];
  });
  orderProductsFullInfo.forEach(product => {
    if (necessaryComboCategories[product.categoryId]) {
      necessaryComboCategories[product.categoryId].right += 1;
    }
    if (product.comboCategoryId !== "") {
      necessaryComboCategories[product.comboCategoryId].left += 1;
    }
  });
  delete necessaryComboCategories[""];
  comboCategories = [];
  necessaryCategoryIds = Object.keys(necessaryComboCategories);
  let messageParts = necessaryCategoryIds.map((necessaryCategoryId, index) => {
    let categoryName = "";
    if (
      necessaryComboCategories[necessaryCategoryId].left >
      necessaryComboCategories[necessaryCategoryId].right
    ) {
      context.categories.map(categoryInfo => {
        if (categoryInfo._id == necessaryCategoryId) {
          categoryName = categoryInfo.categoryName;
          comboCategories.push(categoryInfo);
        }
      });
    }
    return categoryName !== ""
      ? "\nНе забудьте взять " + categoryName + "\n"
      : "";
  });
  return {
    message: messageParts.join("") + "\n",
    categoriesIds: comboCategories
  };
}

const stateMachineOptions = {
  actions: {
    newUser_messageGenerate: assign({
      message: (context, event) => {
        console.log("newUser_messageGenerate", event);
        let message =
          "Добрый день! Это приложение для заказа обедов.\n" +
          'Кафе ТЕПЛЫЙ ПРИЕМ, столовая "Матаф" Штабная 60а';
        return message;
      }
    }),
    newUser_keyboardGenerate: assign({
      keyboard: (context, event) => {
        console.log("newUser_keyboardGenerate", event);
        return {
          Type: "keyboard",
          Buttons: [
            {
              Columns: 6,
              Rows: 2,
              Text: "Начать пользоваться",
              TextSize: "small",
              TextHAlign: "center",
              TextVAlign: "middle",
              ActionType: "reply",
              ActionBody: '{"type": "SUBSCRIBE"}',
              BgColor: "#f7bb3f"
            }
          ]
        };
      }
    }),
    MainMenuActive_messageGenerate: assign({
      message: (context, event) => {
        console.log("MainMenuActive_messageGenerate", event);

        let message = [];
        if (context.categories.length > 0) {
          message = context.categories.map(category => {
            let counter = 0;
            let categoryProducts = context.activeProducts.filter(product => {
              return product.categoryId == category._id;
            });

            categoryMenuArray = categoryProducts.map(product => {
              let leftDelimetr = product.options.length > 0 ? " (" : "";
              let rigthDelimetr = product.options.length > 0 ? " ) - " : " - ";
              counter += 1;
              return (
                counter +
                ". " +
                product.name +
                leftDelimetr +
                product.options.join(", ") +
                rigthDelimetr +
                product.price +
                " руб.\n"
              );
            });
            return (
              category.categoryName.toUpperCase() +
              "\n" +
              categoryMenuArray.join("") +
              "------------------------\n"
            );
          });
        }

        return (
          'Кафе ТЕПЛЫЙ ПРИЕМ, столовая "Матаф" Штабная 60а\n_________________\nМЕНЮ\n------------------------\n' +
          message.join("")
        );
      }
    }),
    MainMenuActive_keyboardGenerate: assign({
      keyboard: (context, event) => {
        console.log("MainMenuActive_keyboardGenerate", event);
        return {
          Type: "keyboard",
          Buttons: [
            {
              Columns: 6,
              Rows: 1,
              Text: "<b>Создать новый заказ</b>",
              TextSize: "small",
              TextHAlign: "center",
              TextVAlign: "middle",
              ActionType: "reply",
              ActionBody: '{"type": "CREATE_ORDER"}',
              BgColor: "#006600"
            },
            {
              Columns: 6,
              Rows: 1,
              Text: "<b>Актульное меню</b>",
              TextSize: "small",
              TextHAlign: "center",
              TextVAlign: "middle",
              ActionType: "reply",
              ActionBody: '{"type": "GET_CURRENT_MENU"}',
              BgColor: "#f7bb3f"
            },
            {
              Columns: 6,
              Rows: 1,
              Text: "<b>Мои заказы</b>",
              TextSize: "small",
              TextHAlign: "center",
              TextVAlign: "middle",
              ActionType: "reply",
              ActionBody: '{"type": "SHOW_CONFIRMED_ORDERS"}',
              BgColor: "#f7bb3f"
            }
          ]
        };
      }
    }),
    order_messageGenerate: assign({
      message: (context, event) => {
        console.log("order_messageGenerate", event);
        let message = "";
        if (context.orderDraft.positions.length > 0) {
          let orderAmount = 0;
          message = "ВАША КОРЗИНА:\n\n";
          let positionsTexts = context.orderDraft.positions.map(
            (position, index) => {
              let product = {};
              let products = context.activeProducts.filter(product => {
                // == instead === is important
                return product._id == position.productId;
              });
              if (products.length === 1) {
                product = products[0];
              }
              let name = product.name ? product.name : "Пусто";
              let option = product.options[position.option]
                ? product.options[position.option]
                : "";
              let price = product.price ? product.price : 0;
              let quantity = position.quantity;
              let positionAmount = price * quantity;
              orderAmount += positionAmount;
              return (
                index +
                1 +
                ". " +
                name +
                " " +
                option +
                "/" +
                price +
                " руб./" +
                quantity +
                "шт./" +
                positionAmount +
                " руб.\n ------\n"
              );
            }
          );
          message +=
            positionsTexts.join("") + "ИТОГО: " + orderAmount + " руб.\n\n";
          message += createComboCategoryRecomendation(context).message;
          message +=
            "ВНИМАНИЕ: Заказ не офромлен, а это значит что мы его не видим, нажмите кнопку ОФОРМИТЬ";
        } else {
          message = "Ваш заказ пуст добавьте позицию\n";
        }

        return message;
      }
    }),
    order_keyboardGenerate: assign({
      keyboard: (context, event) => {
        console.log("order_keyboardGenerate", event);
        let buttons = [];
        comboCategories = createComboCategoryRecomendation(context)
          .categoriesIds;

        comboCategories.forEach(category => {
          buttons.push({
            Columns: 6,
            Rows: 1,
            Text:
              '<font color="#000000"><b>' +
              "Добавить " +
              category.categoryName +
              "</b></font>",
            TextSize: "small",
            TextHAlign: "center",
            TextVAlign: "middle",
            ActionType: "reply",
            ActionBody: JSON.stringify({
              type: "CHOOSE_CATEGORY",
              category: category
            }),
            BgColor: "#f7bb3f"
          });
        });
        buttons.push({
          Columns: 3,
          Rows: 1,
          Text: '<font color="#000000"><b>Добавить позицию</b></font>',
          TextSize: "small",
          TextHAlign: "center",
          TextVAlign: "middle",
          ActionType: "reply",
          ActionBody: '{"type": "ADD_PRODUCT"}',
          BgColor: "#f7bb3f"
        });
        if (context.orderDraft.positions.length > 0) {
          buttons.push({
            Columns: 3,
            Rows: 1,
            Text: '<font color="#000000"><b>Удалить позицию</b></font>',
            TextSize: "small",
            TextHAlign: "center",
            TextVAlign: "middle",
            ActionType: "reply",
            ActionBody: '{"type": "DELETE_PRODUCT"}',
            BgColor: "#f7bb3f"
          });
        }
        if (context.orderDraft.positions.length > 0) {
          buttons.push({
            Columns: 3,
            Rows: 1,
            Text: '<font color="#000000"><b>Оформить</b></font>',
            TextSize: "small",
            TextHAlign: "center",
            TextVAlign: "middle",
            ActionType: "reply",
            ActionBody: '{"type": "APPLY_ORDER"}',
            BgColor: "#008800"
          });
        }

        buttons.push({
          Columns: 3,
          Rows: 1,
          Text: '<font color="#181818"><b>Отменить заказ</b></font>',
          TextSize: "small",
          TextHAlign: "center",
          TextVAlign: "middle",
          ActionType: "reply",
          ActionBody: '{"type": "CANCEL_ORDER"}',
          BgColor: "#ef6062"
        });
        return {
          Type: "keyboard",
          Buttons: buttons
        };
      }
    }),
    mainMenuPassive_messageGenerate: assign({
      message: (context, event) => {
        console.log("mainMenuPassive_messageGenerate", event);
        let message =
          "НЕТ АКТИВНОГО МЕНЮ В ДАННЫЙ МОМЕНТ\nВремя заказов с 17:00 до 10:30. \n К сожалению время заказов закончилось, ждите публикации активного меню, для создания новго заказа.\n ";
        return message;
      }
    }),
    mainMenuPassive_keyboardGenerate: assign({
      keyboard: (context, event) => {
        console.log("mainMenuPassive_keyboardGenerate", event);
        return {
          Type: "keyboard",
          Buttons: [
            {
              Columns: 6,
              Rows: 1,
              Text:
                '<font color="#000000"><b>Посмотреть актульное меню</b></font>',
              TextSize: "small",
              TextHAlign: "center",
              TextVAlign: "middle",
              ActionType: "reply",
              ActionBody: '{"type": "GET_CURRENT_MENU"}',
              BgColor: "#f7bb3f"
            }
          ]
        };
      }
    }),
    waitingCategoryNumber_messageGenerate: assign({
      message: (context, event) => {
        console.log("waitingCategoryNumber_messageGenerate", event);
        let message =
          "Выберите категорию и нажмите кнопку с её номером\n_________________\n";
        // создаем текст из категорий
        let categoesNamesArray = context.categories.map((category, index) => {
          return index + 1 + ". " + category.categoryName + "\n";
        });
        message += categoesNamesArray.join("");
        return message;
      }
    }),
    waitingCategoryNumber_keyboardGenerate: assign({
      keyboard: (context, event) => {
        console.log("waitingCategoryNumber_keyboardGenerate", event);
        let buttons = context.categories.map((category, index) => {
          return {
            Columns: 1,
            Rows: 1,
            Text: '<font color="#000000"><b>' + (index + 1) + "</b></font>",
            TextSize: "small",
            TextHAlign: "center",
            TextVAlign: "middle",
            ActionType: "reply",
            ActionBody: JSON.stringify({
              type: "CHOOSE_CATEGORY",
              category: category
            }),
            BgColor: "#f7bb3f"
          };
        });
        buttons.push({
          Columns: 2,
          Rows: 1,
          Text: '<font color="#181818"><b>Отмена</b></font>',
          TextSize: "small",
          TextHAlign: "center",
          TextVAlign: "middle",
          ActionType: "reply",
          ActionBody: JSON.stringify({
            type: "CANSEL_CATEGORY"
          }),
          BgColor: "#ef6062"
        });
        return {
          Type: "keyboard",
          Buttons: buttons
        };
      }
    }),
    waitingFoodNumber_messageGenerate: assign({
      message: (context, event) => {
        console.log("waitingFoodNumber_messageGenerate", event);
        let message =
          "Категория " +
          event.category.categoryName +
          "\nВыберите номер и нажмите кнопку\n_________________\n";
        // создаем текст из категорий
        let productsArray = context.activeProducts.filter(product => {
          return product.categoryId === event.category._id;
        });
        productsStrings = productsArray.map((product, index) => {
          let leftDelimetr = product.options.length > 0 ? " (" : "";
          let rigthDelimetr = product.options.length > 0 ? " ) - " : " - ";
          return (
            index +
            1 +
            ". " +
            product.name +
            " " +
            leftDelimetr +
            product.options.join(", ") +
            rigthDelimetr +
            product.price +
            " руб.\n"
          );
        });
        message += productsStrings.join("");
        return message;
      }
    }),
    waitingFoodNumber_keyboardGenerate: assign({
      keyboard: (context, event) => {
        console.log("waitingFoodNumber_keyboardGenerate", event);
        let productsArray = context.activeProducts.filter(product => {
          return product.categoryId === event.category._id;
        });
        let buttons = productsArray.map((product, index) => {
          return {
            Columns: 1,
            Rows: 1,
            Text: '<font color="#000000"><b>' + (index + 1) + "</b></font>",
            TextSize: "small",
            TextHAlign: "center",
            TextVAlign: "middle",
            ActionType: "reply",
            ActionBody: JSON.stringify({
              type: "SEND_FOOD_NUMBER",
              productId: product._id
            }),
            BgColor: "#f7bb3f"
          };
        });
        buttons.push({
          Columns: 2,
          Rows: 1,
          Text: '<font color="#181818"><b>Отмена</b></font>',
          TextSize: "small",
          TextHAlign: "center",
          TextVAlign: "middle",
          ActionType: "reply",
          ActionBody: JSON.stringify({
            type: "CANSEL_CATEGORY"
          }),
          BgColor: "#ef6062"
        });
        return {
          Type: "keyboard",
          Buttons: buttons
        };
      }
    }),
    waitingOption_messageGenerate: assign({
      message: (context, event) => {
        console.log("waitingOption_messageGenerate", event);
        let products = context.activeProducts.filter(product => {
          // == instead === is important
          return product._id == event.productId;
        });
        if (products.length === 1) {
          let product = products[0];

          let message =
            "Вы выбрали " +
            product.name +
            "\nВыберите опции этому товару\n_________________\n";
          // создаем текст из options
          let optionsStrings = [];
          if (product.options.length > 0) {
            optionsStrings = product.options.map((option, index) => {
              return index + 1 + ". " + option + "\n";
            });
          } else {
            optionsStrings = ["У этого товара нет опций нажмите ДАЛЕЕ"];
          }

          message += optionsStrings.join("");
          return message;
        }
        return "Ошибка #510. попробуйте ещё раз";
      },
      prositionDraft: (context, event) => {
        return {
          productId: event.productId,
          option: -1,
          quantity: 0
        };
      }
    }),
    waitingOption_keyboardGenerate: assign({
      keyboard: (context, event) => {
        console.log("waitingOption_keyboardGenerate", event);
        let buttons = [];
        let products = context.activeProducts.filter(product => {
          // == instead === is important
          return product._id == event.productId;
        });
        if (products.length === 1) {
          let product = products[0];
          if (product.options.length > 0) {
            buttons = product.options.map((option, index) => {
              return {
                Columns: 1,
                Rows: 1,
                Text: '<font color="#000000"><b>' + (index + 1) + "</b></font>",
                TextSize: "small",
                TextHAlign: "center",
                TextVAlign: "middle",
                ActionType: "reply",
                ActionBody: JSON.stringify({
                  type: "SEND_OPTION_NUMBER",
                  option: index
                }),
                BgColor: "#f7bb3f"
              };
            });
          } else {
            buttons.push({
              Columns: 2,
              Rows: 1,
              Text: '<font color="#000000"><b>Далее</b></font>',
              TextSize: "small",
              TextHAlign: "center",
              TextVAlign: "middle",
              ActionType: "reply",
              ActionBody: JSON.stringify({
                type: "SEND_OPTION_NUMBER",
                option: -1
              }),
              BgColor: "#f7bb3f"
            });
          }
        }

        buttons.push({
          Columns: 2,
          Rows: 1,
          Text: '<font color="#181818"><b>Отмена</b></font>',
          TextSize: "small",
          TextHAlign: "center",
          TextVAlign: "middle",
          ActionType: "reply",
          ActionBody: JSON.stringify({
            type: "CANSEL_CATEGORY"
          }),
          BgColor: "#ef6062"
        });
        return {
          Type: "keyboard",
          Buttons: buttons
        };
      }
    }),
    waitingQuantity_messageGenerate: assign({
      message: (context, event) => {
        console.log("waitingQuantity_messageGenerate", event);

        let message = "";
        let products = context.activeProducts.filter(product => {
          // == instead === is important
          return product._id == context.prositionDraft.productId;
        });
        if (products.length === 1) {
          let product = products[0];
          message = "Вы выбрали " + product.name + " ";
          message += product.options[event.option]
            ? product.options[event.option]
            : "";
          message += "\n Введите количество нажатием кнопки";
          return message;
        }
        return "Ошибка #512. попробуйте ещё раз";
      },
      prositionDraft: (context, event) => {
        let newPositionDraft = JSON.parse(
          JSON.stringify(context.prositionDraft)
        );

        newPositionDraft.option = event.option + 1 ? event.option : -1;
        return newPositionDraft;
      }
    }),
    waitingQuantity_keyboardGenerate: assign({
      keyboard: (context, event) => {
        console.log("waitingQuantity_keyboardGenerate", event);
        buttonsDefinition = [
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10,
          11,
          12,
          13,
          14,
          15,
          16
        ];
        let buttons = [];
        buttons = buttonsDefinition.map(button => {
          return {
            Columns: 1,
            Rows: 1,
            Text: '<font color="#000000"><b>' + button + "</b></font>",
            TextSize: "small",
            TextHAlign: "center",
            TextVAlign: "middle",
            ActionType: "reply",
            ActionBody: JSON.stringify({
              type: "SEND_QUANTITY",
              quantity: button
            }),
            BgColor: "#f7bb3f"
          };
        });
        buttons.push({
          Columns: 2,
          Rows: 1,
          Text: '<font color="#181818"><b>Отмена</b></font>',
          TextSize: "small",
          TextHAlign: "center",
          TextVAlign: "middle",
          ActionType: "reply",
          ActionBody: JSON.stringify({
            type: "CANSEL_CATEGORY"
          }),
          BgColor: "#ef6062"
        });
        return {
          Type: "keyboard",
          Buttons: buttons
        };
      }
    }),
    addPositionToOrder: assign({
      orderDraft: (context, event) => {
        console.log("=======addPositionToOrder======", event);
        let newPositionDraft = JSON.parse(
          JSON.stringify(context.prositionDraft)
        );
        newPositionDraft.quantity = event.quantity;
        let newOrderDraft = JSON.parse(JSON.stringify(context.orderDraft));
        newOrderDraft.positions.push(newPositionDraft);
        return newOrderDraft;
      }
    }),
    waitOrderPositionNumber_messageGenerate: assign({
      message: (context, event) => {
        return (
          "ВЫБЕРИТЕ НОМЕР ПОЗИЦИИ ДЛЯ УДАЛЕНИЯ\n_________\n" + context.message
        );
      }
    }),
    waitOrderPositionNumber_keyboardGenerate: assign({
      keyboard: (context, event) => {
        let buttons = [];
        buttons = context.orderDraft.positions.map((_, index) => {
          return {
            Columns: 1,
            Rows: 1,
            Text: '<font color="#000000"><b>' + (index + 1) + "</b></font>",
            TextSize: "small",
            TextHAlign: "center",
            TextVAlign: "middle",
            ActionType: "reply",
            ActionBody: JSON.stringify({
              type: "SEND_ORDER_POSITION_NUMBER",
              position: index
            }),
            BgColor: "#f7bb3f"
          };
        });
        buttons.push({
          Columns: 2,
          Rows: 1,
          Text: '<font color="#181818"><b>Отмена</b></font>',
          TextSize: "small",
          TextHAlign: "center",
          TextVAlign: "middle",
          ActionType: "reply",
          ActionBody: JSON.stringify({
            type: "CANSEL_CATEGORY"
          }),
          BgColor: "#ef6062"
        });
        return {
          Type: "keyboard",
          Buttons: buttons
        };
      }
    }),
    deletePositionFromOrder: assign({
      orderDraft: (context, event) => {
        let newOrderDraft = JSON.parse(JSON.stringify(context.orderDraft));
        newOrderDraft.positions.splice(event.position, 1);
        return newOrderDraft;
      }
    }),
    registration_messageGenerate: assign({
      message: (context, event) => {
        return "Прежде чем оформить заказ необходимо внести как минимум 1 номер телефона и 1 адрес доставки.\n_______\n\n Введите номер телефона.";
      }
    }),
    registration_keyboardGenerate: assign({
      keyboard: (context, event) => {
        return {
          Type: "keyboard",
          Buttons: [
            {
              Columns: 6,
              Rows: 1,
              Text:
                '<font color="#181818"><b>Отказаться от регистрации</b></font>',
              TextSize: "small",
              TextHAlign: "center",
              TextVAlign: "middle",
              ActionType: "reply",
              ActionBody: '{"type": "CANCEL_REGISTRATION"}',
              BgColor: "#ef6062"
            }
          ]
        };
      }
    }),
    itIsNotPhoneNumber_messageGenerate: assign({
      message: (context, event) => {
        return "Мы не смогли распознать в этом тексте номер телефона, пожалуйста проверьте номер повторите ввод :-)";
      }
    }),
    addPhoneToContext: assign({
      phones: (context, event) => {
        let newPhones = JSON.parse(JSON.stringify(context.phones));
        let phoneString = event.phone.replace(/[\s-\(\)]/g, "");
        if (
          phoneString.match(
            /((8|\+7)-?)?\(?\d{3,5}\)?-?\d{1}-?\d{1}-?\d{1}-?\d{1}-?\d{1}((-?\d{1})?-?\d{1})?/
          )
        ) {
          newPhones.push(phoneString);
        }
        return newPhones;
      }
    }),
    waitingAddress_messageGenerate: assign({
      message: (context, event) => {
        return "Введите адрес доставки, (на данном этапе достаточно ввести 1 адрес доставки, другие адреcа вы сможете добавить на этапе оформления заказа)";
      }
    }),
    addAddressToContext: assign({
      addresses: (context, event) => {
        let newAddresses = JSON.parse(JSON.stringify(context.addresses));
        newAddresses.push(event.address);
        return newAddresses;
      }
    }),
    waitingAddressNumber_messageGenerate: assign({
      message: (context, event) => {
        let message = "Ваши адреса:\n\n";
        let addressesStrings = context.addresses.map((address, index) => {
          return index + 1 + ". " + address + "\n\n";
        });
        message += addressesStrings.join("");
        return (
          message +
          "выберите номер адреса доставки для текущего заказа или добавьте новый адрес нажав на кнопку ДОБАВИТЬ АДРЕС"
        );
      }
    }),
    waitingAddressNumber_keyboardGenerate: assign({
      keyboard: (context, event) => {
        let buttons = [];
        buttons = context.addresses.map((address, index) => {
          return {
            Columns: 1,
            Rows: 1,
            Text: '<font color="#181818"><b>' + (index + 1) + "</b></font>",
            TextSize: "small",
            TextHAlign: "center",
            TextVAlign: "middle",
            ActionType: "reply",
            ActionBody: JSON.stringify({
              type: "SEND_ADDRESS_NUMBER",
              addressIndex: index
            }),
            BgColor: "#008800"
          };
        });
        buttons.push({
          Columns: 6,
          Rows: 1,
          Text: '<font color="#181818"><b>Добавить другой адрес</b></font>',
          TextSize: "small",
          TextHAlign: "center",
          TextVAlign: "middle",
          ActionType: "reply",
          ActionBody: JSON.stringify({
            type: "CREATE_NEW_ADDRESS"
          }),
          BgColor: "#f7bb3f"
        });
        return {
          Type: "keyboard",
          Buttons: buttons
        };
      }
    }),
    addAddressToOrderDraft: assign({
      orderDraft: (context, event) => {
        let newOrderDraft = JSON.parse(JSON.stringify(context.orderDraft));
        newOrderDraft.address = context.addresses[event.addressIndex];
        newOrderDraft.phone = context.phones[context.phones.length - 1];
        return newOrderDraft;
      }
    }),
    orderConfirmation_messageGenerate: assign({
      message: (context, event) => {
        let message = "Адрес доставки: " + context.orderDraft.address + "\n";
        message += "Телефон: " + context.orderDraft.phone + "\n";
        message += "Счет: \n\n";

        let orderAmount = 0;
        let positionsTexts = context.orderDraft.positions.map(
          (position, index) => {
            let product = {};
            let products = context.activeProducts.filter(product => {
              // == instead === is important
              return product._id == position.productId;
            });
            if (products.length === 1) {
              product = products[0];
            }
            let name = product.name ? product.name : "Пусто";
            let option = product.options[position.option]
              ? product.options[position.option]
              : "";
            let price = product.price ? product.price : 0;
            let quantity = position.quantity;
            let positionAmount = price * quantity;
            orderAmount += positionAmount;
            return (
              index +
              1 +
              ". " +
              name +
              " " +
              option +
              "/" +
              price +
              " руб./" +
              quantity +
              "шт./" +
              positionAmount +
              " руб.\n ------\n"
            );
          }
        );
        message +=
          positionsTexts.join("") + "ИТОГО: " + orderAmount + " руб.\n\n";

        let confirmedExists = isConfirmedOrdersToThisAddressExists(
          context,
          context.orderDraft.address
        );
        if (confirmedExists || orderAmount >= 100) {
          message += "ПРОВЕРЬТЕ ВАШ ЗАКАЗ И НАЖМИТЕ КНОПКУ ПОДТВЕРДИТЬ\n\n";
        } else {
          message +=
            "ВНИМАНИЕ: Сумма заказа должна быть минимум 100 рублей. или у вас уже должен быть оформлен заказ на сумму 100 рублей чтоб иметь возможность сделать дополнительный заказ с любой суммой.";
        }

        return message;
      }
    }),
    orderConfirmation_keyboardGenerate: assign({
      keyboard: (context, event) => {
        let confirmedExists = isConfirmedOrdersToThisAddressExists(
          context,
          context.orderDraft.address
        );
        let orderAmount = 0;
        if (context.orderDraft.positions.length > 0) {
          context.orderDraft.positions.map(position => {
            let product = {};
            let products = context.activeProducts.filter(product => {
              // == instead === is important
              return product._id == position.productId;
            });
            if (products.length === 1) {
              product = products[0];
            }
            let price = product.price ? product.price : 0;
            let quantity = position.quantity;
            let positionAmount = price * quantity;
            orderAmount += positionAmount;
          });
        }
        let buttons = [];
        if (confirmedExists || orderAmount >= 100) {
          buttons.push({
            Columns: 6,
            Rows: 2,
            Text: '<font color="#000000"><b>Подтвердить заказ</b></font>',
            TextSize: "small",
            TextHAlign: "center",
            TextVAlign: "middle",
            ActionType: "reply",
            ActionBody: JSON.stringify({
              type: "CONFIRM_ORDER"
            }),
            BgColor: "#008800"
          });
        }

        buttons.push({
          Columns: 3,
          Rows: 1,
          Text: '<font color="#181818"><b>Изменить заказ</b></font>',
          TextSize: "small",
          TextHAlign: "center",
          TextVAlign: "middle",
          ActionType: "reply",
          ActionBody: JSON.stringify({
            type: "CANSEL_CATEGORY"
          }),
          BgColor: "#f7bb3f"
        });

        buttons.push({
          Columns: 3,
          Rows: 1,
          Text: '<font color="#181818"><b>Отменить заказ</b></font>',
          TextSize: "small",
          TextHAlign: "center",
          TextVAlign: "middle",
          ActionType: "reply",
          ActionBody: JSON.stringify({
            type: "CANCEL_ORDER"
          }),
          BgColor: "#ef6062"
        });

        return {
          Type: "keyboard",
          Buttons: buttons
        };
      }
    }),
    orderDarftBecomesConfirmed: assign({
      confirmedOrder: (context, event) => {
        context.orderDraft.timestamp = new Date().getTime();
        return context.orderDraft;
      },
      message: (context, event) => {
        return "Заказ потвержден";
      }
    }),
    addNewAddressToContext: assign({
      addresses: (context, event) => {
        let newAddresses = context.addresses;
        newAddresses.push(event.address);
        return newAddresses;
      }
    }),
    waitingAddressString_messageGenerate: assign({
      message: (context, event) => {
        return "Введите новый адрес. Он добавиться к списку ваших адресов.";
      }
    }),
    waitingAddressString_keyboardGenerate: assign({
      keyboard: (context, event) => {
        let buttons = [
          {
            Columns: 6,
            Rows: 1,
            Text:
              '<font color="#181818"><b>Отменить ввод нового адреса</b></font>',
            TextSize: "small",
            TextHAlign: "center",
            TextVAlign: "middle",
            ActionType: "reply",
            ActionBody: JSON.stringify({
              type: "CANCEL_CREATING_ADDRESS"
            }),
            BgColor: "#f7bb3f"
          }
        ];
        return {
          Type: "keyboard",
          Buttons: buttons
        };
      }
    }),
    confirmedOrders_messageGenerate: assign({
      message: (context, event) => {
        let message = "";
        if (context.orders.length > 0) {
          let ordersStrings = context.orders.map((order, index) => {
            let orderAmount = 0;
            let positionsTexts = order.positions.map((position, index) => {
              let product = {};
              let products = context.activeProducts.filter(product => {
                // == instead === is important
                return product._id == position.productId;
              });
              if (products.length === 1) {
                product = products[0];
              }
              let name = product.name ? product.name : "Пусто";
              let option = product.options[position.option]
                ? product.options[position.option]
                : "";
              let price = product.price ? product.price : 0;
              let quantity = position.quantity;
              let positionAmount = price * quantity;
              orderAmount += positionAmount;
              return (
                index +
                1 +
                ". " +
                name +
                " " +
                option +
                "/" +
                price +
                " руб./" +
                quantity +
                "шт./" +
                positionAmount +
                " руб.\n"
              );
            });
            return (
              "______________________________________\n\nЗАКАЗ №" +
              (index + 1) +
              "\n ------\nТЕЛ: " +
              order.phone +
              "\nАДРЕС: " +
              order.address +
              "\n ------\nСЧЕТ: \n" +
              positionsTexts.join("") +
              "------\nИТОГО: " +
              orderAmount +
              " руб.\n"
            );
          });
          message += ordersStrings.join("");
        } else {
          message += "У вас нет подтвержденных заказов.";
        }
        return message;
      }
    }),
    confirmedOrders_keyboardGenerate: assign({
      keyboard: (context, event) => {
        let buttons = [];
        if (false) {
          // if (context.orders.length > 0) {
          buttons.push({
            Columns: 3,
            Rows: 1,
            Text: '<font color="#181818"><b>Изменить заказ</b></font>',
            TextSize: "small",
            TextHAlign: "center",
            TextVAlign: "middle",
            ActionType: "reply",
            ActionBody: JSON.stringify({
              type: "EDIT_ORDER"
            }),
            BgColor: "#f7bb3f"
          });

          buttons.push({
            Columns: 3,
            Rows: 1,
            Text: '<font color="#181818"><b>Удалить заказ</b></font>',
            TextSize: "small",
            TextHAlign: "center",
            TextVAlign: "middle",
            ActionType: "reply",
            ActionBody: JSON.stringify({
              type: "DELETE_ORDER"
            }),
            BgColor: "#f7bb3f"
          });
        }

        buttons.push({
          Columns: 6,
          Rows: 1,
          Text: '<font color="#181818"><b>Назад</b></font>',
          TextSize: "small",
          TextHAlign: "center",
          TextVAlign: "middle",
          ActionType: "reply",
          ActionBody: JSON.stringify({
            type: "CANCEL"
          }),
          BgColor: "#f7bb3f"
        });

        return {
          Type: "keyboard",
          Buttons: buttons
        };
      }
    }),
    waitingOrderNumberForDelete_messageGenerate: assign({
      message: (context, event) => {
        return "Выберите заказ для удаления и нажмите кнопку с его номером. Заказ будет уделен немедленно без возможности восстановить";
      }
    }),
    waitingOrderNumberForDelete_keyboardGenerate: assign({
      keyboard: (context, event) => {
        let buttons = context.orders.map((_, index) => {
          return {
            Columns: 1,
            Rows: 1,
            Text: '<font color="#181818"><b>' + (index + 1) + "</b></font>",
            TextSize: "small",
            TextHAlign: "center",
            TextVAlign: "middle",
            ActionType: "reply",
            ActionBody: JSON.stringify({
              type: "SEND_DELETE_NUMBER",
              deleteOrderIndex: index
            }),
            BgColor: "#f7bb3f"
          };
        });
        buttons.push({
          Columns: 2,
          Rows: 1,
          Text: '<font color="#181818"><b>Назад</b></font>',
          TextSize: "small",
          TextHAlign: "center",
          TextVAlign: "middle",
          ActionType: "reply",
          ActionBody: JSON.stringify({
            type: "CANCEL"
          }),
          BgColor: "#f7bb3f"
        });
        return {
          Type: "keyboard",
          Buttons: buttons
        };
      }
    }),
    markConfirmedOrderForDelete: assign({
      orderForDelete: (context, event) => {
        return event.deleteOrderIndex;
      }
    }),
    waitingOrderNumberForEdit_messageGenerate: assign({
      message: (context, event) => {
        return "Выберите заказ для редактирвоания и нажмите кнопку с его номером. Примечание после редактирования надо снова подтвердить заказ. иначе мы не увидим ваших изменений";
      }
    }),
    waitingOrderNumberForEdit_keyboardGenerate: assign({
      keyboard: (context, event) => {
        let buttons = context.orders.map((_, index) => {
          return {
            Columns: 1,
            Rows: 1,
            Text: '<font color="#181818"><b>' + (index + 1) + "</b></font>",
            TextSize: "small",
            TextHAlign: "center",
            TextVAlign: "middle",
            ActionType: "reply",
            ActionBody: JSON.stringify({
              type: "SEND_EDIT_NUMBER",
              editOrderIndex: index
            }),
            BgColor: "#f7bb3f"
          };
        });
        buttons.push({
          Columns: 2,
          Rows: 1,
          Text: '<font color="#181818"><b>Назад</b></font>',
          TextSize: "small",
          TextHAlign: "center",
          TextVAlign: "middle",
          ActionType: "reply",
          ActionBody: JSON.stringify({
            type: "CANCEL"
          }),
          BgColor: "#f7bb3f"
        });
        return {
          Type: "keyboard",
          Buttons: buttons
        };
      }
    }),
    editOrderToOrderDraft: assign({
      orderForEdit: (context, event) => {
        return event.editOrderIndex;
      },
      orderDraft: (context, event) => {
        return context.orders[event.editOrderIndex];
      }
    })
  },
  activities: {},
  guards: {
    isMenuActive: (context, event) => {
      let isMenuActiveStatus = !context.isMenuPassive;
      // console.log(event, "isMenuActiveStatus ", isMenuActiveStatus);
      return isMenuActiveStatus;
    },
    isMenuPassive: (context, event) => {
      let isMenuPassiveStatus = context.isMenuPassive;
      // console.log(event, "isMenuPassiveStatus ", isMenuPassiveStatus);
      return isMenuPassiveStatus;
    },
    isRegistred: (context, event) => {
      console.log("isRegistred");
      if (context.phones.length === 0 || context.addresses.length === 0) {
        return;
      }
      return true;
    },
    itIsNotPhoneNumber: (context, event) => {
      if (context.phones.length > 0) {
        return false;
      }
      return true;
    },
    isProductWithoutOptions: (context, event) => {
      let products = context.activeProducts.filter(product => {
        // == instead === is important
        return product._id == context.prositionDraft.productId;
      });
      if (products.length === 1) {
        let product = products[0];
        return product.options.length === 0;
      }
      return false;
    }
  },
  services: {}
};

module.exports = { stateMachineOptions };
