const stateMachineDefinition = {
  id: "foodDelivery",
  initial: "newUser",
  context: {
    // preloaded context
    isMenuPassive: false,
    activeProducts: [],
    categories: [],
    orders: [],
    message: "Сообщение по умолчанию",
    keyboard: {
      Type: "keyboard",
      Buttons: [
        {
          Columns: 6,
          Rows: 2,
          Text: '<br><font color="#494E67"><b>Default button</b></font>',
          TextSize: "large",
          TextHAlign: "center",
          TextVAlign: "middle",
          ActionType: "reply",
          ActionBody: '{"type":"DEFAULT_BUTTON_EVENT"}',
          BgColor: "#f7bb3f"
        }
      ]
    },

    //satete machine context
    userName: "",
    phones: [],
    addresses: [],
    viberId: "",
    viberUserName: "",
    orderDraft: {
      userId: "",
      phone: "",
      name: "",
      address: "",
      positions: [],
      archived: false
    },
    confirmedOrder: {},
    prositionDraft: {
      productId: "",
      option: -1,
      quantity: 0
    },
    orderForDelete: -1,
    orderForEdit: -1
  },
  states: {
    newUser: {
      entry: ["newUser_messageGenerate", "newUser_keyboardGenerate"],
      on: {
        SUBSCRIBE: {
          target: "mainMenuActive",
          actions: [
            "MainMenuActive_messageGenerate",
            "MainMenuActive_keyboardGenerate"
          ]
        }
      }
    },
    mainMenuPassive: {
      on: {
        "": {
          target: "mainMenuActive",
          cond: "isMenuActive"
        },
        GET_CURRENT_MENU: {
          target: "mainMenuActive",
          actions: [
            "MainMenuActive_messageGenerate",
            "MainMenuActive_keyboardGenerate"
          ]
        }
      }
    },
    mainMenuActive: {
      on: {
        "": {
          target: "mainMenuPassive",
          cond: "isMenuPassive",
          actions: [
            "mainMenuPassive_messageGenerate",
            "mainMenuPassive_keyboardGenerate"
          ]
        },
        GET_CURRENT_MENU: {
          target: "mainMenuActive",
          actions: [
            "MainMenuActive_messageGenerate",
            "MainMenuActive_keyboardGenerate"
          ]
        },
        CREATE_ORDER: {
          target: "registration",
          actions: [
            "registration_messageGenerate",
            "registration_keyboardGenerate"
          ]
        },
        SHOW_CONFIRMED_ORDERS: {
          target: "confirmedOrders",
          actions: [
            "confirmedOrders_messageGenerate",
            "confirmedOrders_keyboardGenerate"
          ]
        }
      }
    },
    confirmedOrders: {
      id: "confirmedOrders",
      initial: "confirmedOrdersMainMenuActive",
      states: {
        confirmedOrdersMainMenuActive: {
          on: {
            "": {
              target: "confirmedOrdersMainMenuPassive",
              cond: "isMenuPassive"
              // actions: ["order_messageGenerate", "order_keyboardGenerate"]
            },
            EDIT_ORDER: {
              target: "waitingOrderNumberForEdit",
              actions: [
                "waitingOrderNumberForEdit_messageGenerate",
                "waitingOrderNumberForEdit_keyboardGenerate"
              ]
            },
            DELETE_ORDER: {
              target: "waitingOrderNumberForDelete",
              actions: [
                "waitingOrderNumberForDelete_messageGenerate",
                "waitingOrderNumberForDelete_keyboardGenerate"
              ]
            },
            CANCEL: {
              target: "#foodDelivery.mainMenuActive",
              actions: [
                "MainMenuActive_messageGenerate",
                "MainMenuActive_keyboardGenerate"
              ]
            }
          }
        },
        confirmedOrdersMainMenuPassive: {
          on: {
            "": {
              target: "confirmedOrdersMainMenuActive",
              cond: "isMenuActive"
            },
            CANCEL: {
              target: "#foodDelivery.mainMenuActive",
              actions: [
                "MainMenuActive_messageGenerate",
                "MainMenuActive_keyboardGenerate"
              ]
            }
          }
        },
        waitingOrderNumberForEdit: {
          on: {
            "": {
              target: "confirmedOrdersMainMenuPassive",
              cond: "isMenuPassive"
              // actions: ["order_messageGenerate", "order_keyboardGenerate"]
            },
            SEND_EDIT_NUMBER: {
              target: "#order",
              actions: [
                "editOrderToOrderDraft",
                "order_messageGenerate",
                "order_keyboardGenerate"
              ]
            },
            CANCEL: {
              target: "#foodDelivery.mainMenuActive",
              actions: [
                "MainMenuActive_messageGenerate",
                "MainMenuActive_keyboardGenerate"
              ]
            }
          }
        },
        waitingOrderNumberForDelete: {
          on: {
            "": {
              target: "confirmedOrdersMainMenuPassive",
              cond: "isMenuPassive"
              // actions: ["order_messageGenerate", "order_keyboardGenerate"]
            },
            SEND_DELETE_NUMBER: {
              target: "#foodDelivery.mainMenuActive",
              actions: [
                "MainMenuActive_messageGenerate",
                "MainMenuActive_keyboardGenerate",
                "markConfirmedOrderForDelete"
              ]
            },
            CANCEL: {
              target: "#foodDelivery.mainMenuActive",
              actions: [
                "MainMenuActive_messageGenerate",
                "MainMenuActive_keyboardGenerate"
              ]
            }
          }
        }
      }
    },
    registration: {
      id: "registration",
      initial: "waitingPhone",
      states: {
        waitingPhone: {
          on: {
            "": {
              target: "#order",
              cond: "isRegistred",
              actions: ["order_messageGenerate", "order_keyboardGenerate"]
            },
            SEND_PHONE: {
              target: "waitingAddress",
              actions: [
                "addPhoneToContext",
                "waitingAddress_messageGenerate",
                "registration_keyboardGenerate"
              ]
            },
            CANCEL_REGISTRATION: {
              target: "#foodDelivery.mainMenuActive",
              actions: [
                "MainMenuActive_messageGenerate",
                "MainMenuActive_keyboardGenerate"
              ]
            }
          }
        },
        waitingAddress: {
          on: {
            "": {
              target: "waitingPhone",
              cond: "itIsNotPhoneNumber",
              actions: ["itIsNotPhoneNumber_messageGenerate"]
            },
            SEND_ADDRESS: {
              target: "#order",
              actions: [
                "addAddressToContext",
                "order_messageGenerate",
                "order_keyboardGenerate"
              ]
            },
            CANCEL_REGISTRATION: {
              target: "#foodDelivery.mainMenuActive",
              actions: [
                "MainMenuActive_messageGenerate",
                "MainMenuActive_keyboardGenerate"
              ]
            }
          }
        }
      }
    },
    order: {
      id: "order",
      initial: "orderMainMenu",
      states: {
        orderMainMenu: {
          on: {
            "": {
              target: "#foodDelivery.mainMenuPassive",
              cond: "isMenuPassive",
              actions: [
                "mainMenuPassive_messageGenerate",
                "mainMenuPassive_keyboardGenerate"
              ]
            },
            ADD_PRODUCT: {
              target: "waitingCategoryNumber",
              actions: [
                "waitingCategoryNumber_messageGenerate",
                "waitingCategoryNumber_keyboardGenerate"
              ]
            },
            DELETE_PRODUCT: {
              target: "waitOrderPositionNumber",
              actions: [
                "waitOrderPositionNumber_messageGenerate",
                "waitOrderPositionNumber_keyboardGenerate"
              ]
            },
            CANCEL_ORDER: {
              target: "#foodDelivery.mainMenuActive",
              actions: [
                "MainMenuActive_messageGenerate",
                "MainMenuActive_keyboardGenerate"
              ]
            },
            APPLY_ORDER: {
              target: "waitingAddressNumber",
              actions: [
                "waitingAddressNumber_messageGenerate",
                "waitingAddressNumber_keyboardGenerate"
              ]
            },
            CHOOSE_CATEGORY: {
              target: "waitingFoodNumber",
              actions: [
                "waitingFoodNumber_messageGenerate",
                "waitingFoodNumber_keyboardGenerate"
              ]
            }
          }
        },
        waitOrderPositionNumber: {
          on: {
            "": {
              target: "#foodDelivery.mainMenuPassive",
              cond: "isMenuPassive",
              actions: [
                "mainMenuPassive_messageGenerate",
                "mainMenuPassive_keyboardGenerate"
              ]
            },
            SEND_ORDER_POSITION_NUMBER: {
              //TODO change actions functions
              target: "orderMainMenu",
              actions: [
                "deletePositionFromOrder",
                "order_messageGenerate",
                "order_keyboardGenerate"
              ]
            },
            CANSEL_CATEGORY: {
              target: "orderMainMenu",
              actions: ["order_messageGenerate", "order_keyboardGenerate"]
            }
          }
        },
        waitingCategoryNumber: {
          on: {
            "": {
              target: "#foodDelivery.mainMenuPassive",
              cond: "isMenuPassive",
              actions: [
                "mainMenuPassive_messageGenerate",
                "mainMenuPassive_keyboardGenerate"
              ]
            },
            CHOOSE_CATEGORY: {
              target: "waitingFoodNumber",
              actions: [
                "waitingFoodNumber_messageGenerate",
                "waitingFoodNumber_keyboardGenerate"
              ]
            },
            CANSEL_CATEGORY: {
              target: "orderMainMenu",
              actions: ["order_messageGenerate", "order_keyboardGenerate"]
            },
            CANCEL_ORDER: {
              target: "#foodDelivery.mainMenuActive",
              actions: [
                "MainMenuActive_messageGenerate",
                "MainMenuActive_keyboardGenerate"
              ]
            }
          }
        },
        waitingFoodNumber: {
          on: {
            "": {
              target: "#foodDelivery.mainMenuPassive",
              cond: "isMenuPassive",
              actions: [
                "mainMenuPassive_messageGenerate",
                "mainMenuPassive_keyboardGenerate"
              ]
            },
            SEND_FOOD_NUMBER: {
              target: "waitingOption",
              actions: [
                "waitingOption_messageGenerate",
                "waitingOption_keyboardGenerate"
              ]
            },
            CANSEL_CATEGORY: {
              target: "orderMainMenu",
              actions: ["order_messageGenerate", "order_keyboardGenerate"]
            },
            CANCEL_ORDER: {
              target: "#foodDelivery.mainMenuActive",
              actions: [
                "MainMenuActive_messageGenerate",
                "MainMenuActive_keyboardGenerate"
              ]
            }
          }
        },
        waitingOption: {
          on: {
            "": {
              target: "waitingQuantity",
              cond: "isProductWithoutOptions",
              actions: [
                "waitingQuantity_messageGenerate",
                "waitingQuantity_keyboardGenerate"
              ]
            },
            SEND_OPTION_NUMBER: {
              target: "waitingQuantity",
              actions: [
                "waitingQuantity_messageGenerate",
                "waitingQuantity_keyboardGenerate"
              ]
            },
            CANSEL_CATEGORY: {
              target: "orderMainMenu",
              actions: ["order_messageGenerate", "order_keyboardGenerate"]
            },
            CANCEL_ORDER: {
              target: "#foodDelivery.mainMenuActive",
              actions: [
                "MainMenuActive_messageGenerate",
                "MainMenuActive_keyboardGenerate"
              ]
            }
          }
        },
        waitingQuantity: {
          on: {
            "": {
              target: "#foodDelivery.mainMenuPassive",
              cond: "isMenuPassive",
              actions: [
                "mainMenuPassive_messageGenerate",
                "mainMenuPassive_keyboardGenerate"
              ]
            },
            SEND_QUANTITY: {
              target: "orderMainMenu",
              actions: [
                "addPositionToOrder",
                "order_messageGenerate",
                "order_keyboardGenerate"
              ]
            },
            CANSEL_CATEGORY: {
              target: "orderMainMenu",
              actions: ["order_messageGenerate", "order_keyboardGenerate"]
            },
            CANCEL_ORDER: {
              target: "#foodDelivery.mainMenuActive",
              actions: [
                "MainMenuActive_messageGenerate",
                "MainMenuActive_keyboardGenerate"
              ]
            }
          }
        },
        waitingAddressNumber: {
          on: {
            SEND_ADDRESS_NUMBER: {
              target: "orderConfirmation",
              actions: [
                "addAddressToOrderDraft",
                "orderConfirmation_messageGenerate",
                "orderConfirmation_keyboardGenerate"
              ]
            },
            CREATE_NEW_ADDRESS: {
              target: "waitingAddressString",
              actions: [
                "waitingAddressString_messageGenerate",
                "waitingAddressString_keyboardGenerate"
              ]
            }
          }
        },
        waitingAddressString: {
          on: {
            SEND_ADDRESS_STRING: {
              target: "waitingAddressNumber",
              actions: [
                "addNewAddressToContext",
                "waitingAddressNumber_messageGenerate",
                "waitingAddressNumber_keyboardGenerate"
              ]
            },
            CANCEL_CREATING_ADDRESS: {
              target: "waitingAddressNumber",
              actions: [
                "waitingAddressNumber_messageGenerate",
                "waitingAddressNumber_keyboardGenerate"
              ]
            }
          }
        },
        orderConfirmation: {
          on: {
            CONFIRM_ORDER: {
              target: "#foodDelivery.mainMenuActive",
              actions: [
                "orderDarftBecomesConfirmed",
                "MainMenuActive_keyboardGenerate"
              ]
            },
            CANSEL_CATEGORY: {
              target: "orderMainMenu",
              actions: ["order_messageGenerate", "order_keyboardGenerate"]
            },
            CANCEL_ORDER: {
              target: "#foodDelivery.mainMenuActive",
              actions: [
                "MainMenuActive_messageGenerate",
                "MainMenuActive_keyboardGenerate"
              ]
            }
          }
        }
      }
    }
  }
};

module.exports = { stateMachineDefinition };
