module.exports = {
  db: {
    mongoOptions: { useNewUrlParser: true, useUnifiedTopology: true },
    url: "mongodb://localhost:27017",
    dbName: "FoodDelivery"
  },
  appDefaultConfig: { menuIsPassive: true }
};
