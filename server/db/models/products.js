const getActiveProducts = async function() {
  return new Promise((resolve, reject) => {
    let db = getDb();
    const productsCollection = db.collection("products");
    productsCollection.find({ isActive: true }).toArray((err, products) => {
      if (err) {
        console.log("ERROR getActiveProducts", err);
        reject({ error: err });
      }
      resolve(products);
    });
  }).catch(err => {
    console.log("catch ERROR getActiveProducts", err);
  });
};

module.exports = { getActiveProducts };
