async function getAllCategories(db, callback) {
  return new Promise((resolve, reject) => {
    let db = getDb();
    const categoriesCollection = db.collection("categories");
    categoriesCollection.find({}).toArray((err, categories) => {
      if (err) {
        console.log("ERROR getActiveProducts", err);
        reject({ error: err });
      }
      resolve(categories);
    });
  });
}

module.exports = { getAllCategories };
