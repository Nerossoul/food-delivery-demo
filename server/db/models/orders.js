async function saveNewOrder(newOrderObject) {
  return new Promise(async (resolve, reject) => {
    let db = getDb();
    if (newOrderObject._id) {
      delete newOrderObject._id;
    }
    const ordersCollection = db.collection("orders");
    ordersCollection.insertOne(newOrderObject, (err, result) => {
      if (err) {
        reject(err);
      }
      resolve(true);
    });
  }).catch(err => {
    console.log("ERROR saveNewOrder", err);
  });
}

const getActiveOrders = async function(userId) {
  return new Promise((resolve, reject) => {
    let db = getDb();
    let filter = {};
    if (userId) {
      filter = {
        userId: ObjectID(userId),
        archived: false
      };
    } else {
      filter = {
        archived: false
      };
    }

    const ordersCollection = db.collection("orders");
    ordersCollection
      .find(filter)
      .sort({ timestamp: 1 })
      .toArray((err, orders) => {
        if (err) {
          console.log("ERROR getActiveOrders", err);
          reject({ error: err });
        }
        resolve(orders);
      });
  }).catch(err => {
    console.log("catch ERROR getActiveOrders", err);
  });
};

const deleteOrder = async function(orderId) {
  return new Promise((resolve, reject) => {
    let db = getDb();
    let orderToDelete = { _id: ObjectID(orderId) };
    const ordersCollection = db.collection("orders");
    ordersCollection.deleteOne(orderToDelete).then(
      result => {
        resolve(true);
      },
      err => {
        console.log("ERROR deleteOrder", err);
        reject(false);
      }
    );
  }).catch(err => {
    console.log("catch ERROR deleteOrder", err);
    return false;
  });
};

const updateOrder = async function(orderId, editedOrderData) {
  return new Promise((resolve, reject) => {
    let db = getDb();
    let orderToUpdate = { _id: ObjectID(orderId) };
    let orderUpdateData = { $set: editedOrderData };
    const ordersCollection = db.collection("orders");
    ordersCollection.updateOne(orderToUpdate, orderUpdateData).then(
      result => {
        resolve(true);
      },
      err => {
        console.log("ERROR updateOrder", err);
        reject(false);
      }
    );
  }).catch(err => {
    console.log("catch ERROR updateOrder", err);
    return false;
  });
};

const archiveAllOrders = async function() {
  return new Promise((resolve, reject) => {
    let db = getDb();
    let orderToUpdate = {};
    let orderUpdateData = { $set: { archived: true } };
    const ordersCollection = db.collection("orders");
    ordersCollection.updateMany(orderToUpdate, orderUpdateData).then(
      result => {
        console.log(result);
        resolve(true);
      },
      err => {
        console.log("ERROR archiveAllOrders", err);
        reject(false);
      }
    );
  }).catch(err => {
    console.log("catch ERROR archiveAllOrders", err);
    return false;
  });
};

module.exports = {
  saveNewOrder,
  getActiveOrders,
  deleteOrder,
  updateOrder,
  archiveAllOrders
};
