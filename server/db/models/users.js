async function createNewUser(newUserObject) {
  return new Promise(async (resolve, reject) => {
    let db = getDb();
    const userCollection = db.collection("users");
    userCollection.insertOne(newUserObject, (err, result) => {
      if (err) {
        reject(false);
      }
      resolve(true);
    });
  });
}

async function updateUserByViberId(updateObject) {
  return new Promise((resolve, reject) => {
    let db = getDb();
    let userToUpdate = { viberId: updateObject.viberId };
    let userUpdateData = { $set: updateObject.newUserData };
    const usersCollection = db.collection("users");
    usersCollection
      .updateOne(userToUpdate, userUpdateData)
      .then(
        function(result) {
          console.log("User updated");
          resolve(true);
        },
        function(error) {
          console.log("User not updated");
          console.log(error);
          reject(false);
        }
      )
      .catch(err => console.log("ERROR updating user data", err));
  }).catch(err => console.log("ERROR updateUserByViberId", err));
}

async function getUserByViberId(viberId) {
  return new Promise((resolve, reject) => {
    let db = getDb();
    const usersCollection = db.collection("users");
    let users = usersCollection
      .find({ viberId: viberId })
      .toArray((err, users) => {
        if (err) {
          reject({ error: err });
        }
        resolve({ users });
      });
  });
}

async function getUsers(viberId) {
  return new Promise((resolve, reject) => {
    let db = getDb();
    const usersCollection = db.collection("users");
    let users = usersCollection.find({}).toArray((err, users) => {
      if (err) {
        reject({ error: err });
      }
      resolve(users);
    });
  }).catch(err => console.log(err));
}

async function cleanOrderDraftPositionsInViberStateContextOfEachUser() {
  let users = await getUsers();
  if (users.error) {
    return false;
  }
  let bulkWriteCommandsArray = users.map(user => {
    console.log(user._id);
    let parsedViberState = JSON.parse(user.viberState);

    let newViberState = ":-)";
    if (parsedViberState) {
      console.log(parsedViberState.context.orderDraft.positions);
      parsedViberState.context.orderDraft.positions = [];
      newViberState = JSON.stringify(parsedViberState);
    } else {
      newViberState = user.viberState;
    }
    return {
      updateOne: {
        filter: { _id: ObjectID(user._id) },
        update: { $set: { viberState: newViberState } }
      }
    };
  });
  return new Promise((resolve, reject) => {
    let db = getDb();
    const usersCollection = db.collection("users");
    usersCollection
      .bulkWrite(bulkWriteCommandsArray)
      .then(
        function(result) {
          console.log("Users updated");
          resolve(true);
        },
        function(error) {
          console.log("Users not updated");
          console.log(error);
          reject(false);
        }
      )
      .catch(err => console.log("ERROR updating user data", err));
  }).catch(err => console.log("ERROR updateUserByViberId", err));
}

module.exports = {
  createNewUser,
  getUserByViberId,
  updateUserByViberId,
  getUsers,
  cleanOrderDraftPositionsInViberStateContextOfEachUser
};
