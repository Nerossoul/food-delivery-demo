const assert = require("assert");
const appDefaultConfig = require("../../config").appDefaultConfig;

async function getIsMenuPassiveStatus() {
  let config = await getCurrentConfig(getDb(), () => {});
  if (config.error) {
    return config;
  }
  return config[0].menuIsPassive;
}

async function createDefaultConfig(db) {
  const configCollection = db.collection("config");
  await configCollection.insertOne(appDefaultConfig, function(err, result) {
    assert.equal(err, null);
  });
  console.log("New CONFIG saved to base");
  return [appDefaultConfig];
}

async function getCurrentConfig(db, callback) {
  return new Promise(function(resolve, reject) {
    const configCollection = db.collection("config");
    configCollection.find({}).toArray(function(err, config) {
      if (err) {
        reject({ error: err });
      }
      if (config.length > 0) {
        callback(config);
        resolve(config);
      } else {
        createDefaultConfig(getDb()).then(appDefaultConfig => {
          console.log("-->", appDefaultConfig);
          callback(appDefaultConfig);
          console.log("Send CONFIG to client");
          resolve(appDefaultConfig);
        });
      }
    });
  });
}
const changeConfig = function(db, updateObj, callback) {
  let configToUpdate = { _id: ObjectID(updateObj._id) };

  let configUpdateData = { $set: updateObj.newConfigData };
  const configCollection = db.collection("config");
  configCollection
    .updateOne(configToUpdate, configUpdateData)
    .then(function(result) {
      console.log("CONFIG updated");
      callback(result);
    });
};

module.exports = {
  getIsMenuPassiveStatus,
  changeConfig,
  getCurrentConfig
};
