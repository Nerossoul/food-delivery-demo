const MongoClient = require("mongodb").MongoClient;
const ObjectID = require("mongodb").ObjectID;
const assert = require("assert");
const config = require("../config");
// Create a new MongoClient
const client = new MongoClient(config.db.url, config.db.mongoOptions);

let _db;

function initDb(callback) {
  if (_db) {
    console.warn("Trying to init DB again!");
    return callback(null, _db);
  }
  // Use connect method to connect to the Server
  client.connect(function(err) {
    assert.equal(null, err);
    console.log("Connected successfully to database server");
    _db = client.db(config.db.dbName);
    // client.close();
    callback(err);
  });
}

function getDb() {
  assert.ok(_db, "Db has not been initialized. Please called init first.");
  return _db;
}

module.exports = { getDb, initDb, ObjectID };
