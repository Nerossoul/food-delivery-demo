const ngrok = require("../../controllers/helpers/get_public_url.js");
const ViberBot = require("viber-bot").Bot;
const BotEvents = require("viber-bot").Events;

// Creating the bot with access token, name and avatar

const bot = new ViberBot({
  authToken: "4a800335c5a7d641-125a1b81a9e0a5b-ddd47ae35ca935c5",
  name: "FoodDelivery",
  avatar: ""
});

let viberWebhookUrl = "";
ngrok
  .getPublicUrl()
  .then(publicUrl => {
    viberWebhookUrl = publicUrl + "/api/viber";
    console.log("Set the new viber webhook address", viberWebhookUrl);
    return bot.setWebhook(viberWebhookUrl);
  })
  .then(setWebHookResult => {
    console.log(setWebHookResult);
    return true;
  })
  .catch(error => {
    console.log("Can not connect to ngrok server. Is it running?");
    console.error(error);
  });

bot.on(BotEvents.MESSAGE_RECEIVED, (message, response) => {
  // Echo's back the message to the client. Your bot logic should sit here.
  response.send(message);
});
let viberBotMiddleware = bot.middleware();

module.exports = { viberBotMiddleware };
