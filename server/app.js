global.initDb = require("./db/db.js").initDb;
global.getDb = require("./db/db.js").getDb;
global.ObjectID = require("./db/db.js").ObjectID;

const express = require("express");
const cors = require("cors");

const app = express();
app.use(express.json());
app.use(cors());

// routes
const auth = require("./routes/api/auth");
const categories = require("./routes/api/categories");
const products = require("./routes/api/products");
const viber = require("./routes/api/viber");
const config = require("./routes/api/config");
const orders = require("./routes/api/orders");

app.use("/api/viber", viber);
app.use("/api/auth", auth);
app.use("/api/categories", categories);
app.use("/api/products", products);
app.use("/api/config", config);
app.use("/api/orders", orders);

//Handle producion
if (process.env.NODE_ENV === "production") {
  console.log("it is production");
  // Static folder
  app.use(express.static(__dirname + "/public"));

  //Handle SPA
  app.get(/.*/, (req, res) => res.sendFile(__dirname + "/public/index.html"));
} else {
  console.log("development mode");

  app.use(express.static(__dirname + "/public"));

  //Handle SPA
  app.get(/.*/, (req, res) => res.sendFile(__dirname + "/public/index.html"));
}

initDb(async err => {
  const port = process.env.PORT || 5000;
  app.listen(port, function(err) {
    if (err) {
      throw err; //
    }
    console.log(`Server listening port ${port}...`);
  });
});
