const express = require("express");
const router = express.Router();
const assert = require("assert");

//model functions
const saveNewProduct = function(db, newProduct, callback) {
  const productsCollection = db.collection("products");
  productsCollection.insertOne(newProduct, function(err, result) {
    assert.equal(err, null);
    console.log("New product saved to base");
    callback(result);
  });
};
const getAllProducts = function(db, callback) {
  const productsCollection = db.collection("products");
  productsCollection.find({}).toArray(function(err, products) {
    assert.equal(err, null);
    console.log("Send all Products to client");
    callback(products);
  });
};
const updateProduct = function(db, updateObj, callback) {
  let productToUpdate = { _id: ObjectID(updateObj._id) };

  let productUpdateData = { $set: updateObj.newProductData };
  const productsCollection = db.collection("products");
  productsCollection
    .updateOne(productToUpdate, productUpdateData)
    .then(function(result) {
      console.log("Product updated");
      callback(result);
    });
};
const deleteProduct = function(db, productId, callback) {
  let productToDelete = { _id: ObjectID(productId) };
  const productsCollection = db.collection("products");
  productsCollection.deleteOne(productToDelete).then(function(result) {
    console.log("Product deleted");
    callback(result);
  });
};

//routes

//CREATE PRODUCT
router.post("/", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }
  let newProduct = req.body;
  if (newProduct) {
    saveNewProduct(getDb(), newProduct, result => {
      getAllProducts(getDb(), sendResult);
    });
  } else {
    getAllProducts(getDb(), sendResult);
  }
});

//READ PRODUCT
router.get("/", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }
  getAllProducts(getDb(), sendResult);
});

//UPDATE PRODUCT
router.put("/:productId", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }
  let updateObj = { _id: req.params.productId, newProductData: req.body };
  if (req.params.productId && req.body) {
    updateProduct(getDb(), updateObj, result => {
      getAllProducts(getDb(), sendResult);
    });
  } else {
    getAllProducts(getDb(), sendResult);
  }
});

//DELETE PRODUCT
router.delete("/:productId", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }
  let productId = req.params.productId;
  if (productId) {
    deleteProduct(getDb(), productId, result => {
      getAllProducts(getDb(), sendResult);
    });
  } else {
    getAllProducts(getDb(), sendResult);
  }
});

module.exports = router;
