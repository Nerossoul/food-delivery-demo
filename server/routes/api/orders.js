const express = require("express");
const router = express.Router();
const assert = require("assert");
const { getActiveOrders } = require("../../db/models/orders");

//routes

//CREATE ORDER
router.post("/", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }
});

//READ ORDERS
router.get("/:mode", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }
  if ((req.params.mode = "activeOnly")) {
    console.log("WE ARE GETTING ACTIVE ORDERS NOW");
    let activeOrders = await getActiveOrders();
    if (activeOrders.error) {
      console.log("ERROR GETTING ACTIVE ORDERS", activeOrders.error);
      activeOrders = [];
    }
    console.log(activeOrders);
    sendResult(activeOrders);
  } else {
    sendResult("No implementation yet");
  }

  console.log("Orders sent to client");
});

//UPDATE ORDER
router.put("/:productId", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }
  sendResult("access denied");
});

//DELETE ORDER
router.delete("/:productId", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }
  sendResult("access denied");
});

module.exports = router;
