const express = require("express");
const router = express.Router();
const assert = require("assert");

//model functions
const saveNewCategory = function(db, newCategory, callback) {
  const categoriesCollection = db.collection("categories");
  categoriesCollection.insertOne(newCategory, function(err, result) {
    assert.equal(err, null);
    console.log("New Category saved to base");
    callback(result);
  });
};
const getAllCategories = function(db, callback) {
  const categoriesCollection = db.collection("categories");
  categoriesCollection.find({}).toArray(function(err, categories) {
    if (err) {
      console.log("getAllCategories ERROR", err);
      callback([{ categoryName: "ОШИБКА", _id: "0" }]);
    } else {
      console.log("Send all categories to client");
      callback(categories);
    }
  });
};
const updateCategory = function(db, updateObj, callback) {
  let categoryToUpdate = { _id: ObjectID(updateObj._id) };

  let categoryUpdateData = { $set: updateObj.categoryName };
  const categoriesCollection = db.collection("categories");
  categoriesCollection
    .updateOne(categoryToUpdate, categoryUpdateData)
    .then(function(result) {
      console.log("update Category");
      callback(result);
    });
};
const deleteCategory = function(db, categoryId, callback) {
  let categoryToDelete = { _id: ObjectID(categoryId) };
  const categoriesCollection = db.collection("categories");
  categoriesCollection.deleteOne(categoryToDelete).then(function(result) {
    console.log("Category deleted");
    callback(result);
  });
};

//routes

//CREATE CATEGORY
router.post("/", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }
  let newCategory = req.body;
  if (newCategory) {
    saveNewCategory(getDb(), newCategory, result => {
      getAllCategories(getDb(), sendResult);
    });
  } else {
    getAllCategories(getDb(), sendResult);
  }
});

//READ CATEGORIES
router.get("/", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }
  getAllCategories(getDb(), sendResult);
});

//UPDATE CATEGORY
router.put("/:categoryId", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }
  let updateObj = { _id: req.params.categoryId, categoryName: req.body };
  if (req.params.categoryId && req.body) {
    updateCategory(getDb(), updateObj, result => {
      getAllCategories(getDb(), sendResult);
    });
  } else {
    getAllCategories(getDb(), sendResult);
  }
});

//DELETE CATEGORY
router.delete("/:categoryId", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }
  let categoryId = req.params.categoryId;
  if (categoryId) {
    deleteCategory(getDb(), categoryId, result => {
      getAllCategories(getDb(), sendResult);
    });
  } else {
    getAllCategories(getDb(), sendResult);
  }
});

module.exports = router;
