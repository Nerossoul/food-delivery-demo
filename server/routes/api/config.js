const express = require("express");
const router = express.Router();
const getCurrentConfig = require("../../db/models/config").getCurrentConfig;
const changeConfig = require("../../db/models/config").changeConfig;
const { archiveAllOrders } = require("../../db/models/orders");
const {
  cleanOrderDraftPositionsInViberStateContextOfEachUser
} = require("../../db/models/users");

//routes

//CREATE CONFIG
router.post("/", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }
  sendResult({ error: "access denided" });
});

//READ CONFIG
router.get("/", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }
  getCurrentConfig(getDb(), sendResult);
});

//UPDATE CONFIG
router.put("/:configId?", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }
  if (!req.body.menuIsPassive) {
    console.log("CLEAN IT");
    archiveAllOrders();
    cleanOrderDraftPositionsInViberStateContextOfEachUser();
  }
  let updateObj = { _id: req.params.configId, newConfigData: req.body };
  if (req.params.configId && req.body) {
    changeConfig(getDb(), updateObj, result => {
      getCurrentConfig(getDb(), sendResult);
    });
  } else {
    getCurrentConfig(getDb(), sendResult);
  }
});

//DELETE CONFIG
router.delete("/:CONFIGId", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }
  sendResult({ error: "access denided" });
});

module.exports = router;
