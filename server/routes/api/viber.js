const express = require("express");
const router = express.Router();
const applyStateMachineEvent = require("../../controllers/xstate/stateMachine")
  .applyStateMachineEvent;

//routes

//VIBER EVENTS HANDLER
router.post("/:viberEvent", async (req, res) => {
  function sendResult(result) {
    res.send(result);
  }

  let responseObject = await applyStateMachineEvent(
    req.body.userProfile,
    req.body.message
  );
  sendResult(responseObject);
});

module.exports = router;
