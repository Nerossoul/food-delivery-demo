import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import auth from "./auth.module.js";
import categories from "./categories.module.js";
import products from "./products.module.js";
import orders from "./orders.module.js";

Vue.use(Vuex);

const token = localStorage.getItem("user-token");
if (token) {
  axios.defaults.headers.common["Authorization"] = token;
}

export const store = new Vuex.Store({
  state: {
    config: {}
  },
  getters: {
    CONFIG: state => {
      return state.config[0];
    }
  },
  mutations: {
    UPDATE_CONFIG: (state, payload) => {
      state.config = payload;
    }
  },
  actions: {
    GET_CONFIG: context => {
      return new Promise((resolve, reject) => {
        axios
          .get("/api/config")
          .then(resp => {
            if (resp.data) {
              context.commit("UPDATE_CONFIG", resp.data);
              resolve(true);
            } else {
              // eslint-disable-next-line no-console
              console.log(resp);
              reject(false);
            }
          })
          .catch(err => {
            // eslint-disable-next-line no-console
            console.log("error", err);
            reject(false);
          });
      });
    },
    CHANGE_CONFIG: async function(context, updateObj) {
      return new Promise((resolve, reject) => {
        let configData = JSON.parse(JSON.stringify(updateObj));
        delete configData._id;
        axios
          .put("/api/config/" + updateObj._id, configData)
          .then(resp => {
            return resp.data;
          })
          .then(config => {
            context.commit("UPDATE_CONFIG", config);
            resolve(true);
          })
          .catch(err => {
            // eslint-disable-next-line no-console
            console.log(err);
            reject(false);
          });
      });
    }
  },

  modules: {
    auth,
    categories,
    products,
    orders
  }
});
