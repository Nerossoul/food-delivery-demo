import axios from "axios";

const state = {
  status: ""
};

const getters = {
  isAuthenticated: async ({ dispatch }) => dispatch.IS_AUTHENTICATED,
  authStatus: state => state.status
};
const mutations = {
  AUTH_REQUEST: state => {
    state.status = "loading";
  },
  AUTH_SUCCESS: state => {
    state.status = "success";
  },
  AUTH_ERROR: state => {
    state.status = "error";
  },
  AUTH_LOGOUT: state => {
    state.status = "";
  }
};
const actions = {
  AUTH_REQUEST: ({ commit }, user) => {
    return new Promise((resolve, reject) => {
      // The Promise used for router redirect in login
      commit("AUTH_REQUEST");
      axios
        .post("/api/auth", user)
        .then(resp => {
          if (resp.data.token) {
            const token = resp.data.token;
            localStorage.setItem("user-token", token); // store the token in localstorage
            axios.defaults.headers.common["Authorization"] = token;
            commit("AUTH_SUCCESS");
            // you have your token, now log in your user :)
            resolve(resp);
          } else {
            commit("AUTH_ERROR", resp);
            localStorage.removeItem("user-token");
            delete axios.defaults.headers.common["Authorization"];
            reject(resp);
          }
        })
        .catch(err => {
          commit("AUTH_ERROR", err);
          localStorage.removeItem("user-token"); // if the request fails, remove any possible user token if possible
          delete axios.defaults.headers.common["Authorization"];
          reject(err);
        });
    });
  },
  AUTH_LOGOUT: ({ commit }) => {
    return new Promise(resolve => {
      commit("AUTH_LOGOUT");
      localStorage.removeItem("user-token"); // clear your user's token from localstorage
      delete axios.defaults.headers.common["Authorization"];
      resolve();
    });
  },
  IS_AUTHENTICATED: () => {
    return new Promise((resolve, reject) => {
      // The Promise used for router redirect in login
      axios
        .post("/api/auth/?method=isAuth")
        .then(resp => {
          if (resp.data.isAuthenticated === true) {
            resolve(true);
          } else {
            resolve(false);
          }
        })
        .catch(err => {
          reject(err);
        });
    });
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
