import axios from "axios";

const state = {
  categories: []
};

const getters = {
  CATEGORIES: state => {
    return state.categories;
  }
};
const mutations = {
  UPDATE_CATEGORIES: (state, payload) => {
    state.categories = payload;
  }
};
const actions = {
  SAVE_NEW_CATEGORY: (context, payload) => {
    return new Promise((resolve, reject) => {
      axios
        .post("/api/categories", payload)
        .then(resp => {
          if (resp.data) {
            context.commit("UPDATE_CATEGORIES", resp.data);
            resolve(true);
          } else {
            // eslint-disable-next-line no-console
            console.log(resp);

            reject(false);
          }
        })
        .catch(err => {
          // eslint-disable-next-line no-console
          console.log("error", err);
          reject(false);
        });
    });
  },
  GET_ALL_CATEGORIES: context => {
    return new Promise((resolve, reject) => {
      axios
        .get("/api/categories")
        .then(resp => {
          if (resp.data) {
            context.commit("UPDATE_CATEGORIES", resp.data);
            resolve(true);
          } else {
            // eslint-disable-next-line no-console
            console.log(resp);
            reject(false);
          }
        })
        .catch(err => {
          // eslint-disable-next-line no-console
          console.log("error", err);
          reject(false);
        });
    });
  },
  UPDATE_CATEGORY: async function({ commit }, updateObj) {
    return new Promise((resolve, reject) => {
      axios
        .put("/api/categories/" + updateObj._id, {
          categoryName: updateObj.categoryName
        })
        .then(resp => {
          return resp.data;
        })
        .then(categories => {
          commit("UPDATE_CATEGORIES", categories);
          resolve(true);
        })
        .catch(err => {
          // eslint-disable-next-line no-console
          console.log(err);
          reject(false);
        });
    });
  },
  DELETE_CATEGORY: (context, categoryId) => {
    return new Promise((resolve, reject) => {
      let config = {
        data: {}
      };
      axios
        .delete("/api/categories/" + categoryId, config)
        .then(resp => {
          if (resp.data) {
            // eslint-disable-next-line no-console
            console.log(resp.data);
            context.commit("UPDATE_CATEGORIES", resp.data);
            resolve(true);
          } else {
            // eslint-disable-next-line no-console
            console.log(resp);

            reject(false);
          }
        })
        .catch(err => {
          // eslint-disable-next-line no-console
          console.log("error", err);
          reject(false);
        });
    });
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
