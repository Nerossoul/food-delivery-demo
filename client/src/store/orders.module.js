import axios from "axios";

const state = {
  orders: []
};

const getters = {
  ORDERS: state => {
    return state.orders;
  }
};
const mutations = {
  UPDATE_ORDERS: (state, payload) => {
    state.orders = payload;
  }
};
const actions = {
  GET_NOT_ARCHIVED_ORDERS: context => {
    return new Promise((resolve, reject) => {
      axios
        .get("/api/orders/activeOnly")
        .then(resp => {
          if (resp.data) {
            context.commit("UPDATE_ORDERS", resp.data);
            resolve(true);
          } else {
            // eslint-disable-next-line no-console
            console.log(resp);
            reject(false);
          }
        })
        .catch(err => {
          // eslint-disable-next-line no-console
          console.log("error", err);
          reject(false);
        });
    });
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
