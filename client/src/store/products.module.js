import axios from "axios";

const state = {
  products: []
};

const getters = {
  PRODUCTS: state => {
    return state.products;
  }
};
const mutations = {
  UPDATE_PRODUCTS: (state, payload) => {
    state.products = payload;
  }
};
const actions = {
  SAVE_NEW_PRODUCT: (context, payload) => {
    return new Promise((resolve, reject) => {
      axios
        .post("/api/products", payload)
        .then(resp => {
          if (resp.data) {
            context.commit("UPDATE_PRODUCTS", resp.data);
            resolve(true);
          } else {
            // eslint-disable-next-line no-console
            console.log(resp);

            reject(false);
          }
        })
        .catch(err => {
          // eslint-disable-next-line no-console
          console.log("error", err);
          reject(false);
        });
    });
  },
  GET_ALL_PRODUCTS: context => {
    return new Promise((resolve, reject) => {
      axios
        .get("/api/products")
        .then(resp => {
          if (resp.data) {
            context.commit("UPDATE_PRODUCTS", resp.data);
            resolve(true);
          } else {
            // eslint-disable-next-line no-console
            console.log(resp);
            reject(false);
          }
        })
        .catch(err => {
          // eslint-disable-next-line no-console
          console.log("error", err);
          reject(false);
        });
    });
  },
  UPDATE_PRODUCT: async function({ commit }, updateObj) {
    return new Promise((resolve, reject) => {
      let productData = JSON.parse(JSON.stringify(updateObj));
      delete productData._id;
      axios
        .put("/api/products/" + updateObj._id, productData)
        .then(resp => {
          return resp.data;
        })
        .then(products => {
          commit("UPDATE_PRODUCTS", products);
          resolve(true);
        })
        .catch(err => {
          // eslint-disable-next-line no-console
          console.log(err);
          reject(false);
        });
    });
  },
  DELETE_PRODUCT: (context, productId) => {
    return new Promise((resolve, reject) => {
      let config = {
        data: {}
      };
      axios
        .delete("/api/products/" + productId, config)
        .then(resp => {
          if (resp.data) {
            // eslint-disable-next-line no-console
            console.log(resp.data);
            context.commit("UPDATE_PRODUCTS", resp.data);
            resolve(true);
          } else {
            // eslint-disable-next-line no-console
            console.log(resp);

            reject(false);
          }
        })
        .catch(err => {
          // eslint-disable-next-line no-console
          console.log("error", err);
          reject(false);
        });
    });
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
