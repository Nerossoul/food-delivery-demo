import Vue from "vue";
import Vuetify from "vuetify/lib";
import colors from "vuetify/lib/util/colors";

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: "mdi"
  },
  theme: {
    themes: {
      light: {
        primary: "#665CAC",
        secondary: "#4A4A4A",
        accent: "#F4EF7B",
        error: "#EF6062",
        anchor: "#F4EF7B"
      },
      dark: {
        primary: "#665CAC",
        secondary: "#4A4A4A",
        accent: "#F4EF7B",
        error: "#EF6062",
        anchor: "#F4EF7B"
      }
    }
  }
});
