import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  { path: "/", component: () => import("../components/Home") },
  { path: "/login", component: () => import("../components/LoginForm") },
  {
    path: "/dashboard",
    component: () => import("../components/dashboard/Dashboard")
  },
  {
    path: "/categories",
    component: () => import("../components/dashboard/Categories")
  },

  {
    path: "/products/:id?",
    component: () => import("../components/dashboard/Products")
  }
  // {
  //   path: "/newEvent/:edit?",
  //   component: () => import("../components/NewEvent")
  // },

  // { path: "/dashboard", component: () => import("../components/UserDashboard") }
];

const router = new VueRouter({
  mode: "history",
  routes
});

export default router;
